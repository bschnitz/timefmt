//bin/false || /usr/bin/node << //EOF
var timefmt = require('./timefmt').TimeFmt;

f = timefmt.flags;
ms = 11;
console.log(timefmt.format( ms, f.autoshort | f.timecount ));
console.log(timefmt.format( ms*10, f.autoshort | f.timecount ));
console.log(timefmt.format( ms*100, f.autoshort | f.timecount ));
console.log(timefmt.format( ms*100*60, f.autoshort | f.timecount ));
console.log(timefmt.format( ms*100*60*60, f.autoshort | f.timecount ));
console.log(timefmt.format( ms*100*60*60*12, f.autoshort | f.timecount ));

/* expected output
00.011
00.110
01.100
01m:06
01h:06
13h:12
*/

//EOF
