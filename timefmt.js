if( typeof(window) === "undefined" )
  window = {};

var TimeFmt = (function(){
  var TimeFmt = function(){};

  TimeFmt.flags = {
    timestamp : 0x1,
    timecount : 0x2,
    autoshort : 0x4
  }

  /// @function TimeFmt.init
  ///     create a semantic time structure from a timecount in milliseconds
  ///
  /// @param function $set
  ///     the function to set the timer
  /// @param function $clear
  ///     the function to clear the timer (clearInterval/clearTimeout)
  /// @param function $handle
  ///     the callback parameter for the timer function
  /// @param integer $duration
  ///     the milliseconds argument for the timer function
  ///
  /// @return timerep t with
  /// t.ms - milliseconds left to the next full second
  /// t.ss - seconds      left to the next full minute
  /// t.mm - minutes      left to the next full hour
  /// t.hh - hours        left to the next full day 
  /// t.dd - days         left to the next full week
  /// t.ww - weeks        left to the next full year
  ///                     (counting with 52 weeks/year)
  /// t.yy - years
  TimeFmt.time_from_count = function(time_in_ms)
  {
    time = time_in_ms;
    timerep = {};
    timerep.ms = time % 1000; time = Math.floor(time / 1000);
    timerep.ss = time %   60; time = Math.floor(time /   60);
    timerep.mm = time %   60; time = Math.floor(time /   60);
    timerep.hh = time %   24; time = Math.floor(time /   24);
    timerep.dd = time %    7; time = Math.floor(time /    7);
    timerep.ww = time %   52; time = Math.floor(time /   52);
    timerep.yy = time;
    return timerep;
  }

  function pad(pad, number)
  {
    pad = pad.toString();
    number = number.toString();
    if( number.length > pad.length )
      return number

    return pad.slice( 0, pad.length - number.length ) + number;
  }

  TimeFmt.auto_short_str = function(timerep, num_parts)
  {
    timerep = [
      timerep.yy, timerep.ww, timerep.dd,
      timerep.hh, timerep.mm, timerep.ss, timerep.ms
    ];

    sep = [ "y:00", "w:00", "d:00", "h:00", "m:00", ".000" ]

    for( var i = 0; i < timerep.length-2; ++i )
      if( timerep[i] ) break;

    return pad("00", timerep[i]) + pad(sep[i], timerep[i+1])
  }

  TimeFmt.format = function(time_in_ms, flags, format)
  {
    if( (flags & this.flags.timecount) && (flags & this.flags.autoshort) )
      return TimeFmt.auto_short_str( TimeFmt.time_from_count(time_in_ms) );
    else throw "TimeFmt.format: Not implemented yet!";
  }

  return TimeFmt;
})();

if( typeof module !== 'undefined' )
{
  // exports for nodejs
  module.exports = {
    TimeFmt : TimeFmt
  }
}
